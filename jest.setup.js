import '@testing-library/jest-dom/extend-expect'

import { loadEnvConfig } from '@next/env'
const loadEnv = async () => {
  const projectDir = process.cwd()
  loadEnvConfig(projectDir)
}

jest
  .useFakeTimers()
  .setSystemTime(new Date('2020-01-01').getTime())

export default loadEnv