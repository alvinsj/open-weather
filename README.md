This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, install dependencies 

```bash
yarn install
```

Add environment variables
```bash
# .env.local
NEXT_PUBLIC_WEATHER_FORECAST_URL=https://samples.openweathermap.org/data/2.5/forecast?q=M%C3%BCnchen,DE&appid=b6907d289e10d714a6e88b30761fae22
```

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Proxy

CORS request is proxied through `/api/cors?url=[cors request url]`, check out `pages/api/cors` for implementation.

## Development docs  

Please refer to docs/README.md