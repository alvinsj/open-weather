export const toCelcius = (kelvin: number) => {
  return Math.round(kelvin - 273.15)
}

export default toCelcius
