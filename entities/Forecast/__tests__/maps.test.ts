import parseJSON from 'date-fns/parseJSON'
import { mapForecasts, findNowcast, mapForecast, mapIconType } from '../maps'
import { Munich as mock } from '../mocks'
import { Forecast, WeatherType } from '../types'

describe('maps', () => {
  describe('mapIconType', () => {
    it('returns cloud icon as default', () => {
      expect(mapIconType('')).toEqual(WeatherType.cloud)
    })

    it('returns sub icon', () => {
      expect(mapIconType('01d')).toEqual(WeatherType.sun)
    })

    it('returns cloud icon as default', () => {
      expect(mapIconType('02n')).toEqual(WeatherType.cloud)
    })
  })

  describe('findNowcast', () => {
    it('returns 0 when forecast is not found', () => {
      jest.useFakeTimers().setSystemTime(new Date('2017-01-17').getTime())

      const [item, itemIndex] = findNowcast(mock.list as Forecast[])
      expect(parseJSON(item.dt_txt).toISOString()).toEqual(
        '2017-02-16T12:00:00.000Z'
      )
      expect(itemIndex).toEqual(0)
    })

    it('returns 0 when forecast is not found', () => {
      jest.useFakeTimers().setSystemTime(new Date('2017-05-17').getTime())

      const [item, itemIndex] = findNowcast(mock.list as Forecast[])
      expect(parseJSON(item.dt_txt).toISOString()).toEqual(
        '2017-02-16T12:00:00.000Z'
      )
      expect(itemIndex).toEqual(0)
    })

    it('finds the forecast before now', () => {
      jest.useFakeTimers().setSystemTime(new Date('2017-02-17').getTime())

      const [item, itemIndex] = findNowcast(mock.list as Forecast[])
      expect(parseJSON(item.dt_txt).toISOString()).toEqual(
        '2017-02-17T00:00:00.000Z'
      )
      expect(itemIndex).toEqual(4)
    })
  })

  describe('mapForecast', () => {
    it('map one forecast to props', () => {
      expect(mapForecast(mock.list[0] as Forecast, mock.city.name)).toEqual({
        date: '16. Feb',
        day: 'Thursday',
        hour: '13:00',
        icon: 'sun',
        place: 'Altstadt',
        status: 'Clear',
        temp: '14°',
        tempMinMax: '14° / 8°',
      })
    })
  })

  describe('mapForecasts', () => {
    it('maps forecasts from props array', () => {
      expect(
        mapForecasts(mock.list.slice(0, 2) as Forecast[], mock.city.name)
      ).toEqual([
        {
          date: '16. Feb',
          day: 'Thursday',
          hour: '13:00',
          icon: 'sun',
          place: 'Altstadt',
          status: 'Clear',
          temp: '14°',
          tempMinMax: '14° / 8°',
        },
        {
          date: '16. Feb',
          day: 'Thursday',
          hour: '16:00',
          icon: 'sun',
          place: 'Altstadt',
          status: 'Clear',
          temp: '13°',
          tempMinMax: '13° / 9°',
        },
      ])
    })
  })
})
