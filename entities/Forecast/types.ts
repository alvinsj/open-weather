export type Weather = {
  id: number
  main: string
  description: string
  icon: string
}

export type Forecast = {
  dt: number
  main: {
    temp: number
    temp_min: number
    temp_max: number
    pressure: number
    sea_level: number
    grnd_level: number
    humidity: number
    temp_kf: number
  }
  weather: Weather[]
  clouds: {
    all: number
  }
  wind: {
    speed: number
    deg: number
  }
  rain: {
    '3h': number
  }
  snow: {
    '3h': number
  }
  sys: {
    pod: string
  }
  dt_txt: string
}

export enum WeatherType {
  sun = 'sun',
  cloud = 'cloud',
}

export enum IconSize {
  small = 'small',
  medium = 'medium',
  big = 'big',
}
