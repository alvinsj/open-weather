import { Forecast, WeatherType } from './types'
import toCelcius from '../../utils/toCelcius'
import format from 'date-fns/format'
import parseJSON from 'date-fns/parseJSON'
import isAfter from 'date-fns/isAfter'

// FIXME samples api does not return recent dates, so only hour is checked
export const findNowcast = (forecasts: Forecast[]): [Forecast, number] => {
  const now = new Date()

  // find forecast with date after now
  let foundIndex = forecasts.findIndex((item) =>
    isAfter(parseJSON(item.dt_txt), now)
  )

  // backtrack one step as we want the forecast before now, e.g. 3 hours interval
  foundIndex = foundIndex < 1 ? 0 : foundIndex - 1

  return [forecasts[foundIndex], foundIndex]
}

export const mapIconType = (icon: string) => {
  switch (icon) {
    case '01d':
      return WeatherType.sun
    // FIXME get more icons
    default:
    case '02n':
      return WeatherType.cloud
  }
}

export const mapForecasts = (forecasts: Forecast[], city: string) => {
  return forecasts.map((item: Forecast) => mapForecast(item, city))
}

export const mapForecast = (forecast: Forecast, city: string) => {
  const { dt_txt, main, weather } = forecast

  return {
    icon: mapIconType(weather[0].icon),
    temp: `${toCelcius(main.temp)}°`,
    status: weather[0].main,
    tempMinMax: `${toCelcius(main.temp_max)}° / ${toCelcius(main.temp_min)}°`,
    place: city,
    day: parseJSON(dt_txt).toLocaleDateString('en-US', {
      weekday: 'long',
    }),
    date: format(parseJSON(dt_txt), 'dd. MMM'),
    hour: format(parseJSON(dt_txt), 'HH:mm'),
  }
}
