/* eslint-disable @next/next/no-img-element */
import { FC } from 'react'

import { WeatherType } from '../entities/Forecast/types'

export type WeatherIconProps = {
  type: WeatherType
  className?: string
}

const WeatherIcon: FC<WeatherIconProps> = ({ type, className }) => {
  return {
    [WeatherType.sun]: (
      <img alt="Sunshine time" src="/weather-sun.svg" className={className} />
    ),
    [WeatherType.cloud]: (
      <img alt="Cloudy time" src="/weather-cloud.svg" className={className} />
    ),
  }[type]
}
export default WeatherIcon
