import React from 'react'
import { render, cleanup } from '@testing-library/react'
import { /*axe,*/ toHaveNoViolations } from 'jest-axe'
import WeatherWidget from '../index'
import useWeatherForecast from '../../../hooks/useWeatherForecast'

expect.extend(toHaveNoViolations)

jest.mock('../../../hooks/useWeatherForecast', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    weatherForecast: require('../../../entities/Forecast/mocks').Munich,
    isLoading: false,
    isError: false,
  })),
}))

describe('WeatherWidget', () => {
  afterEach(() => {
    cleanup()
  })

  it('renders', () => {
    const { container } = render(<WeatherWidget />)
    expect(container).toMatchSnapshot()
  })

  it('renders when it is loading', () => {
    ;(useWeatherForecast as jest.Mock).mockReturnValue({
      weatherForecast: null,
      isLoading: true,
      isError: false,
    })

    const { container } = render(<WeatherWidget />)
    expect(container).toMatchSnapshot()
  })

  it('renders error', () => {
    ;(useWeatherForecast as jest.Mock).mockReturnValue({
      weatherForecast: null,
      isLoading: false,
      isError: true,
    })
    const { container } = render(<WeatherWidget />)
    expect(container).toMatchSnapshot()
  })

  it('renders error when response is unexpected', () => {
    ;(useWeatherForecast as jest.Mock).mockReturnValue({
      weatherForecast: {},
      isLoading: false,
      isError: false,
    })
    const { container } = render(<WeatherWidget />)
    expect(container).toMatchSnapshot()
  })

  // it('should have no accessibility violations', async () => {
  //   const { container } = render(<img />)

  //   const results = await axe(container)
  //   expect(results).toHaveNoViolations()
  // })
})
