/* eslint-disable @next/next/no-img-element */
import { FC, useMemo } from 'react'
import format from 'date-fns/format'

import styles from './styles.module.scss'
import { Forecast, WeatherType } from '../../entities/Forecast/types'
import {
  mapForecasts,
  findNowcast,
  mapForecast,
} from '../../entities/Forecast/maps'
import useWeatherForecast from '../../hooks/useWeatherForecast'
import WeatherIcon from '../WeatherIcon'

const initialNowcast = {
  icon: WeatherType.sun,
  temp: `0°`,
  status: 'Unknown',
  tempMinMax: `N/A`,
  place: 'Unknown',
  day: new Date().toLocaleDateString('en-US', {
    weekday: 'long',
  }),
  date: format(new Date(), 'dd. MMM'),
  hour: format(new Date(), 'HH:mm'),
}
const initialCasts = [initialNowcast]

export type WeatherWidgetProps = { className?: string }

const WeatherWidget: FC<WeatherWidgetProps> = ({ className }) => {
  const { weatherForecast, isLoading, isError } = useWeatherForecast()
  const isInvalidResponse =
    !isLoading && (!weatherForecast || !('list' in weatherForecast))

  const { now, casts } = useMemo(() => {
    if (isLoading || isError || isInvalidResponse) {
      return { now: initialNowcast, casts: initialCasts }
    }

    const [nowcast, nowIndex] = findNowcast(weatherForecast.list as Forecast[])
    return {
      now: mapForecast(nowcast, weatherForecast.city.name),
      casts: mapForecasts(
        weatherForecast.list.slice(nowIndex) as Forecast[],
        weatherForecast.city.name
      ),
    }
  }, [isError, isInvalidResponse, isLoading, weatherForecast])

  if (isError || isInvalidResponse)
    return <div>Error loading weather forecast</div>

  return (
    <article className={className}>
      <section className={styles.summary}>
        <WeatherIcon type={now.icon} className={styles.summary_icon} />
        <div className={styles.summary_feel}>
          <div className={styles.feel_sky}>{now.status}</div>
          <div className={styles.feel_tempToday}>{now.tempMinMax}</div>
          <div className={styles.feel_tempCurrent}>{now.temp}</div>
        </div>
        <div className={styles.summary_details}>
          <div className={styles.details_place}>{now.place}</div>
          <div className={styles.details_dateDay}>{now.day}</div>
          <div className={styles.details_dateDate}>{now.date}</div>
        </div>
      </section>

      <ul className={styles.forecast}>
        {casts.map((item, i) => (
          <li key={`forecast-item-${i}`} className={styles.forecast_item}>
            <div className={styles.forecastItem_hour}>{item.hour}</div>
            <WeatherIcon
              type={item.icon}
              className={styles.forecastItem_icon}
            />
            <div className={styles.forecastItem_temp}>{item.temp}</div>
          </li>
        ))}
      </ul>
    </article>
  )
}

export default WeatherWidget
