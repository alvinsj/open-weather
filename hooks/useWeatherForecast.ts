import useSWR from 'swr'
import { corsFetcher } from '../services/fetcher'

export const useWeatherForecast = () => {
  const { data, error } = useSWR(
    process.env.NEXT_PUBLIC_WEATHER_FORECAST_URL,
    corsFetcher
  )

  return {
    weatherForecast: data,
    isLoading: !error && !data,
    isError: error,
  }
}

export default useWeatherForecast
