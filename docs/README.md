# Development

## File structure

Files are organized:
- `pages/*`: entry pages
- `styles/*`: styles 
- `components/*`: react components
- `hooks/*`: react hooks
- `entities/*`: domain entities
- `services/*`: API services, analytics, error reporting etc
- `utils/*`: misc helpers

## CSS/SASS 

CSS/SASS are organized:
- Use `BEM` naming/methodology, e.g. `Block_element__modifier`.
- Do not use style inheritance, e.g. `.parent .children`.
- Use `CSS variables` to organize values, e.g. `var(--primaryColor)`.
- Use `mixins` to organize groups, e.g. no `@extend`.
- Use `clamp(min, desired, max)` to achieve dynamic sizing.
- Use `CSS Grid` for layouting.
- Styles should be simple and portable in future.

## React Components 

Components are organized:
- Use flat structure in render tree, e.g. improve visibility of the render tree.
- Do not create extra/nested components for only structure. e.g. only for complex components with single responsibility.
- Use native CSS and do not use extra divs for layouting, e.g. no helper like Grid/Row/Column components.
- Separate layout styles vs. component styles, e.g. layout = where + outer spacing, component = styles/spacing of child elements.
- Use `<img/>` tag for `SVG`s to make dynamic size.
- Use `custom hook` or simply `map` functions to organize computation/transformation of properties. e.g. improve testability.
- Create mocks to use it for initial development, later also useful for test (the quick one).

## Features 
- Use `entities/types` to organize domain entities, reuseable for other projects.
- Use API proxy to safely prevent CORS issue on browsers.
