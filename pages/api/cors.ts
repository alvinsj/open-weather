/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextApiRequest, NextApiResponse } from 'next'

const Cors = async (req: NextApiRequest, res: NextApiResponse) => {
  let { url } = req.query
  try {
    url = typeof url === 'string' ? url : url[0]
    const resProxy = await fetch(url)
    res.status(200).send(resProxy.body)
  } catch (error: any) {
    res.status(400).send(error.toString())
  }
}

export default Cors
