/* eslint-disable @typescript-eslint/no-explicit-any */
export const corsFetcher = (url: string, ...rest: any[]) =>
  fetch(`/api/cors?url=${encodeURIComponent(url)}`, ...rest).then(
    (res: { json: () => any }) => res.json()
  )

export const fetcher = (url: string, ...args: any[]) =>
  fetch(url, ...args).then((res: { json: () => any }) => res.json())

export default fetcher
